// soal 1
const keliling_persegi_panjang = (panjang, lebar) => {
    let keliling = 2 * (panjang + lebar);
    console.log(keliling);
} 

keliling_persegi_panjang(10, 5);

// soal 2
const myFuncton = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function(){
          console.log(firstName + " " + lastName)
        }
      }
}
newFunction("William", "Imoh").fullName() 

// soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby)

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)

// soal 5
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}` 

console.log(before)